package task_03_Arrays_Collections1;

import java.util.HashSet;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.Set;

class TestProgramm implements TaskThree {

    @Override
    public void makePriorityQueue() {
        PriorityQueue<String> priorityQueue = new PriorityQueue<String>();
        priorityQueue.add("Piter");
        priorityQueue.add("Heris");
        priorityQueue.add("John");
        priorityQueue.add("Pitt");
        priorityQueue.add("George");
        priorityQueue.add("Kith");

        /*
         * Print the most priority element
         */
        System.out.println("Head value using peek method: " + priorityQueue.peek());

        /*
         * Print all elements
         */
        System.out.println("The elements of queue: ");
        for (String name : priorityQueue) {
            System.out.print(name);
        }

        /*
         * Removing the top proiority element and printing the modified priority queue
         * using poll()method
         */
        priorityQueue.poll();
        System.out.println("After removing an element ");
        Iterator<String> iterator = priorityQueue.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
        /*
         * Removing some element using remove() and printing the modified priority queue
         * using poll()method
         */
        priorityQueue.remove("George");
        System.out.println("Queue after removing element: George");
        for (String name : priorityQueue) {
            System.out.println(name);
        }
        /*
         * Check if an element is present using contains()
         */
        boolean checkingElement = priorityQueue.contains("John");
        System.out.println("Priority queue contains element John: " + checkingElement);
        /*
         * Getting objects from queue using toArray() in an array and print array
         */
        Object[] arrayOfObjects = priorityQueue.toArray();
        System.out.println("Array values:");
        for (int i = 0; i < arrayOfObjects.length; i++) {
            System.out.println("Value: " + arrayOfObjects[i].toString());
        }
    }

    @Override
    public void arraySameElements(int[] arrayOne, int[] arrayTwo) {
        Set<Integer> sameValues = new HashSet<Integer>();
        Set<Integer> differentValues = new HashSet<Integer>();
        int sameValue = 0;
        int differentValue = 0;
        for (int i = 0; i < arrayOne.length; i++) {
            for (int j = 0; j < arrayTwo.length; j++) {
                if (arrayOne[i] == arrayTwo[j]) {
                    sameValue = arrayOne[i];
                    sameValues.add(sameValue);
                }
            }
        }
        for (int k = 0; k < arrayOne.length; k++) {
            for (int n = 0; n < arrayTwo.length; n++) {
                if (arrayOne[k] != arrayTwo[n]) {
                    differentValue = arrayOne[k];
                    differentValues.add(differentValue);
                }
            }
        }
        Object arraySameElements[] = sameValues.toArray();
        System.out.println("The array of same values:");
        for (int i = 0; i < arraySameElements.length; i++) {
            System.out.print(arraySameElements[i] + " ");
        }
        Object arrayDifferentElements[] = differentValues.toArray();
        System.out.println("\nThe array of different values:");
        for (int i = 0; i < arrayDifferentElements.length; i++) {
            System.out.print(arrayDifferentElements[i] + " ");

        }
    }

    @Override
    public void arrayDeleteCaples(int[] array) {
        Set<Integer> uniqueElements = new HashSet<Integer>();
        int elementUnique = 0;

        for (int i = 0; i < array.length; i++) {
            if (array[i] == array[i]) {
                elementUnique = array[i];
                uniqueElements.add(elementUnique);
            }
        }
        System.out.println("Array without couples:");
        Object[] arrayOfUniqueElements = uniqueElements.toArray();
        for (int i = 0; i < arrayOfUniqueElements.length; i++) {
            System.out.print(arrayOfUniqueElements[i] + " ");
        }
    }

    @Override
    public void arrayDeleteSequanseOfElementsAndLeaveOne(int[] array) {
        Set<Integer> leaveOne = new HashSet<Integer>();
        int sameElement = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == array[j]) {
                    sameElement = array[i];
                    leaveOne.add(sameElement);
                }
            }
        }
        System.out.println("One same element on the sequance in array: ");
        for (Integer element : leaveOne) {
            System.out.print(element + " ");
        }
    }

    @Override
    public void game() {
        System.out.println("=======Welcome to game:=======");
        System.out.println("You have a hero with 25 helth points.");
        int i;
        int health = 25;
        int artefact = 0;
        int monstrHealth = 0;
        int counterOfDoors = 0;
        Set<Integer> doorsForWinning = new HashSet<Integer>();
        int countOfDoors = 0;
        System.out.println("He start moving.....");
        int[] roomWithTenDoors = new int[10];
        for (i = 0; i < roomWithTenDoors.length; i++) {
            roomWithTenDoors[i] = (int) ((Math.random() * 3) - 1);
            if (roomWithTenDoors[i] == 1) {
                monstrHealth = (int) (Math.random() * 100) - 5;
                System.out.println("Under the door number " + i + " hero will found monstr ");
            } else if (roomWithTenDoors[i] == 0) {
                artefact = (int) ((10 + Math.random() * 70));
                System.out.println("Under the door number " + i + " hero will found artefact");
            }
        }
        System.out.println();
        System.out.println("====Table presentation====");
        System.out.println("--------------------------");
        for (i = 0; i < roomWithTenDoors.length; i++) {
            if (roomWithTenDoors[i] == 1) {
                System.out.println("Door number " + i + "| monstr    |");
                System.out.println("--------------------------");
            } else if (roomWithTenDoors[i] == 0) {
                doorsForWinning.add(i);
                System.out.println("Door number " + i + "| artefact  |");
                System.out.println("--------------------------");
            }
        }
        System.out.println("Doors were hero may dead: ");
        for (i = 0; i < roomWithTenDoors.length; i++) {
            counterOfDoors = countingOfPosibleDeath(health, 0, roomWithTenDoors);
        }

        System.out.println(counterOfDoors);

        System.out.println("Doprs sequence of win: ");
        for (Integer door : doorsForWinning) {
            System.out.print(door + " ");
        }
        // For game play code:

        // for (i = 0; i < roomWithTenDors.length; i++) {
        //
        // if (roomWithTenDors[i] == 0) {
        // artefact = (int) ((10 + Math.random() * 70));
        // health += artefact;
        // System.out.println(
        // "Hero found magic artefact:" + artefact + " plus to health " + " now his
        // health is " + health);
        // } else if (roomWithTenDors[i] == 1) {
        // monstrHealth = (int) (Math.random() * 100) - 5;
        //
        // if (monstrHealth < health) {
        // health -= monstrHealth;
        // System.out.println("Hero fight with monstr and lost " + monstrHealth
        // + " points of health now his health " + health);
        // } else if (monstrHealth > health) {
        // System.out.println("Hero lose figting with monstr.");
        // return;
        // } else if (monstrHealth == health) {
        // System.out.println("Hero win.");
        // continue;
        // } else if (health <= 0) {
        // System.out.println("Hero win.");
        // break;
        // }
        // }
        // }
    }

    public static int countingOfPosibleDeath(int health, int doorNumber, int roomWithTenDoors[]) {
        while (doorNumber <= 9) {
            int healthAfterTheDoor = roomWithTenDoors[doorNumber];
            if (health < healthAfterTheDoor) {
                return countingOfPosibleDeath(health, ++doorNumber, roomWithTenDoors) + 1;
            } else {
                return countingOfPosibleDeath(health, ++doorNumber, roomWithTenDoors);
            }
        }
        return 0;
    }
}
