package task_03_Arrays_Collections1;

public interface TaskThree{
        public void makePriorityQueue();

        public void arraySameElements(int arrayOne[], int arrayTwo[]);

        public void arrayDeleteCaples(int array[]);

        public void arrayDeleteSequanseOfElementsAndLeaveOne(int array[]);

        public void game();
}
